'use strict'

const { src, dest, watch, series, parallel } = require('gulp')
const del = require('del')
const minifyCSS = require('gulp-minify-css')
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const sass = require('gulp-sass')
const terser = require('gulp-terser')
const autoprefixer = require('gulp-autoprefixer')
const htmlreplace = require('gulp-html-replace')
const htmlmin = require('gulp-htmlmin')
const mustache = require('gulp-mustache')
const gulpif = require('gulp-if')

const srcFolder = './src/'
const distFolder = './public/'
const reportsFolder = './src/reports/'

const minify = true

function clean () {
  return del([
    distFolder + 'js',
    distFolder + 'css',
    distFolder + 'index.html',
    distFolder + 'reports'
  ])
}

function cssVendor () {
  return src(srcFolder + 'sass/*.scss')
    .pipe(sass({ style: 'compressed', errLogToConsole: true }))
    .pipe(dest(srcFolder + 'css'))
}

function cssMinify () {
  return src(srcFolder + 'css/*.css')
    .pipe(autoprefixer())
    .pipe(gulpif(minify, minifyCSS()))
    .pipe(concat('all.css'))
    .pipe(gulpif(minify, rename('all.min.css')))
    .pipe(dest(distFolder + 'css'))
}

function jsVendor () {
  return src([
    'node_modules/materialize-css/js/cash.js',
    'node_modules/materialize-css/js/component.js',
    'node_modules/materialize-css/js/global.js',
    'node_modules/materialize-css/js/anime.min.js',
    'node_modules/materialize-css/js/collapsible.js',
    'node_modules/materialize-css/js/dropdown.js',
    'node_modules/materialize-css/js/waves.js',
    'node_modules/materialize-css/js/sidenav.js',
    'node_modules/materialize-css/js/cards.js',
    'node_modules/materialize-css/js/buttons.js'
  ])
    .pipe(concat('vendor.js'))
    .pipe(gulpif(minify, terser()))
    .pipe(dest(srcFolder + 'js'))
}

function jsMinify () {
  return src([
    srcFolder + 'js/vendor.js',
    srcFolder + 'js/app.js'
  ])
    .pipe(gulpif(minify, terser()))
    .pipe(concat('all.js'))
    .pipe(gulpif(minify, rename('all.min.js')))
    .pipe(dest(distFolder + 'js'))
}

function htmlMenu () {
  return src(srcFolder + 'templates/sidenav.mustache')
    .pipe(mustache(srcFolder + 'data/menu.json'))
    .pipe(rename('sidenav_filled.mustache'))
    .pipe(dest(srcFolder + 'templates'))
}

function html () {
  return src(srcFolder + 'templates/index.html')
    .pipe(mustache(srcFolder + 'data/data.json'))
    .pipe(htmlreplace({
      css: minify ? 'css/all.min.css' : 'css/all.css',
      js: minify ? 'js/all.min.js' : 'js/all.js'
    }))
    .pipe(gulpif(minify, htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      removeOptionalTags: true,
      minifyJS: true,
      processScripts: ['text/template', 'text/javascript']
    })))
    .pipe(rename('index.html'))
    .pipe(dest(distFolder))
}

function reportsCssMinify () {
  return src(reportsFolder + 'report-*/**/*.css')
    .pipe(minifyCSS())
    .pipe(dest(distFolder + 'reports'))
}

function reportsJsMinify () {
  return src(reportsFolder + 'report-*/**/*.js')
    .pipe(terser())
    .pipe(dest(distFolder + 'reports'))
}

function reportsStatic () {
  return src([
    reportsFolder + '**',
    reportsFolder + '**/.css/*',
    reportsFolder + '**/.js/*',
    reportsFolder + '**/.icons/*',
    '!' + reportsFolder + '**/*.less',
    '!' + reportsFolder + '**/*.map'
  ])
    .pipe(dest(distFolder + 'reports'))
}

exports.clean = clean

exports.css = series(cssVendor, cssMinify)
exports.js = series(jsMinify, jsVendor)
exports.html = series(htmlMenu, html)

exports.min = parallel(
  series(cssVendor, cssMinify),
  series(jsVendor, jsMinify),
  html
)

exports.watch = function () {
  watch(srcFolder + 'css/*.css', cssMinify)
  watch(srcFolder + 'sass/*.scss', cssVendor)
  watch(srcFolder + 'js/*.js', jsMinify)
  watch(srcFolder + 'templates/*', html)
  watch(srcFolder + 'data/menu.json', htmlMenu)
  watch(srcFolder + 'data/data.json', html)
}

exports.buildDev = series(
  clean,
  parallel(
    series(cssVendor, cssMinify),
    series(jsVendor, jsMinify),
    html
  )
)

exports.build = series(
  clean,
  parallel(
    series(cssVendor, cssMinify),
    series(jsVendor, jsMinify),
    html
  ),
  reportsStatic,
  parallel(
    reportsCssMinify,
    reportsJsMinify
  )
)
