# !/bin/sh
set -e

jobs=[]

if [ ! -z ${MY_GITLAB_TOKEN} ]; then
  full_jobs=$(curl --header "PRIVATE-TOKEN: $MY_GITLAB_TOKEN" $CI_API_V4_URL/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs)
  jobs=$(echo $full_jobs | jq '[.[] | {id: .name, gid: .id, stage: .stage, status: .status, duration: .duration, url: .web_url}]')
fi

dirs=$(cd src/reports; ls -d report-* | jq -R -s -c 'split("\n")[:-1]');

full_dirs=$(cd src/reports; tree -J --dirsfirst -f --noreport -I "." -I "index.html|README.md");

vars=$(jq -n env | sed '/TOKEN/d' | sed '/email/d' | sed '/JWT/d');

echo "{\"jobs\":"$jobs",\"dirs\":"$dirs",\"full_dirs\":"$full_dirs",\"vars\":"$vars"}"