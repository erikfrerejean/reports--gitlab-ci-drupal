(function (window, document) {
  'use strict'

  /**
   * jobs statuses.
   */
  const statuses = {
    success: {
      color: 'green-text',
      text: 'done'
    },
    failed: {
      color: 'red-text',
      text: 'report'
    },
    canceled: {
      color: 'orange-text',
      text: 'cancel'
    },
    skipped: {
      color: '',
      text: 'more_horiz'
    }
  }

  /**
   * Instantiate the report.
   */
  const initApp = () => {
    dropdownInit()
    sidenavInit()
    loadHome()
    handleStaticMenu()
    handleMenu()
  }

  /**
   *
   */
  const loadHome = () => {
    const template = document.querySelector('#home')
    document.getElementById('main').innerHTML = template.innerHTML
    show('#main')
  }

  /**
   * Init the dropdown.
   */
  const dropdownInit = () => {
    // Init dropdown.
    var elements = document.querySelectorAll('.collapsible')
    // eslint-disable-next-line no-undef
    const dropInstances = M.Collapsible.init(elements)

    return dropInstances
  }

  /**
   * Prepare and init the home content.
   */
  const show = (id) => {
    var content = document.querySelector(id)
    if (content) {
      if (content.classList.contains('hide')) {
        content.classList.remove('hide')
      }
    }
  }

  /**
   * Handle static pages links.
   */
  const handleStaticMenu = () => {
    const menus = document.querySelectorAll('a.static');
    [].forEach.call(menus, (menu) => {
      menu.addEventListener('click', () => {
        const templateId = menu.getAttribute('href')
        resetReport()
        show('#main')
        const template = document.querySelector(templateId)
        document.getElementById('main').innerHTML = template.innerHTML
      })
    })
  }

  /**
   * Empty iframe src.
   */
  const resetReport = () => {
    document.querySelector('#report').setAttribute('src', '')
  }

  /**
   * Fill iframe with source.
   *
   * @param String src
   */
  const setReport = (src) => {
    document.querySelector('#report').setAttribute('src', src)
  }

  /**
   * Prepare and init the sidenav.
   */
  const sidenavInit = () => {
    var sidenav = document.querySelector('#sidenav')
    // eslint-disable-next-line
    var instances = M.Sidenav.init(sidenav)

    const toggleSidenav = document.querySelector('#toggle_sidenav')

    toggleSidenav.addEventListener('click', function () {
      // Help to resize body to 100%.
      document.querySelector('body').classList.toggle('no-sidebar')

      // Keep sidenav open on load.
      const sidenav = document.querySelector('#sidenav')
      // eslint-disable-next-line no-undef
      const sidenavInstance = M.Sidenav.getInstance(sidenav)
      if (sidenavInstance.isOpen) {
        sidenavInstance.close()
      } else {
        sidenavInstance.open()
      }
    })
  }

  /**
   * Filter menu by existing jobs and set status.
   * @TODO: clean sub jobs if not on TOOLS_QA or TOOLS_METRICS / unit-kernel if no coverage
   */
  const handleMenu = () => {
    const menus = document.querySelectorAll('a.job');

    [].forEach.call(menus, (menu) => {
      // Add menu click actions to populate iframe.
      bindMenu(menu)
      // Filter the menu list with status.
      setMenuStatus(menu)
    })
  }

  /**
   *
   * @param {*} menu
   */
  const setMenuStatus = (menu) => {
    const jobId = menu.getAttribute('data-job')
    // eslint-disable-next-line no-undef
    const jobs = dataJobs || {}
    // eslint-disable-next-line no-undef
    const dirs = dataDirs || []

    // Filter existing jobs.
    if (jobs[jobId] !== undefined && jobId !== null) {
      var jobStatus = statuses[jobs[jobId].status]
      var jobMarkup = menu.querySelector('.job-status')

      // If we have a dirs, mean we have file for this job.
      if (dirs.includes('report-' + jobId) !== false) {
        jobMarkup.classList.add(jobStatus.color)
        jobMarkup.innerHTML = jobStatus.text
      }

    } else {
      // If we have a dirs, mean we have file for this job.
      if (dirs.includes('report-' + jobId) !== false) {
        var jobMarkup = menu.querySelector('.job-status')
        jobMarkup.innerHTML = ''
      }
      else {
        menu.parentNode.remove()
      }
    }
  }

  /**
   *
   * @param {*} menu
   */
  const bindMenu = (menu) => {
    menu.addEventListener(
      'click',
      () => {
        // Hide no job page.
        document.querySelector('#main').setAttribute('class', 'hide');
        // Clean other active.
        [].forEach.call(document.querySelectorAll('li.active'), (li) => {
          li.removeAttribute('class')
        })
        // Set li active.
        menu.parentNode.setAttribute('class', 'active blue lighten-2')
        // Populate the iframe.
        setReport(menu.getAttribute('data-report'))
      },
      false
    )
  }

  document.addEventListener('DOMContentLoaded', () => {
    initApp()
  })
})(this, this.document)
